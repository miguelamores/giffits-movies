# giffits-movies

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

create a .env file in root directory and add this:
VUE_APP_API="http://www.omdbapi.com/"

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
