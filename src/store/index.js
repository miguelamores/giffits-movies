import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    movies: [],
    sortBy: "Year",
    movie: null,
    loading: false,
    error: null
  },
  mutations: {
    GET_MOVIE_LIST(state, movies) {
      state.movies = movies;
    },
    SET_SORT(state, sortBy) {
      state.sortBy = sortBy;
    },
    GET_MOVIE_DETAIL(state, movie) {
      state.movie = movie;
    },
    SET_LOADING(state) {
      state.loading = !state.loading;
    },
    SET_ERROR(state, error) {
      state.error = error;
    }
  },
  actions: {
    loadMovies({ commit }, params) {
      commit("SET_ERROR", null);
      commit("SET_LOADING");
      fetch(
        `${process.env.VUE_APP_API}?s=${params.phrase}&apikey=e7d2d6f9&type=movie`,
        {
          method: "GET"
        }
      )
        .then(res => res.json())
        .then(res => {
          if (res.Response === "True") {
            commit("GET_MOVIE_LIST", res.Search);
            commit("SET_SORT", params.sortBy);
          } else {
            commit("GET_MOVIE_LIST", []);
            commit("SET_ERROR", res.Error);
          }
          commit("SET_LOADING");
        })
        .catch(() => commit("SET_ERROR", "Unexpected error"));
    },
    getMovie({ commit }, movieId) {
      commit("SET_LOADING");
      fetch(`${process.env.VUE_APP_API}?i=${movieId}&apikey=e7d2d6f9`)
        .then(res => res.json())
        .then(res => {
          if (res.Response === "True") {
            commit("GET_MOVIE_DETAIL", res);
          } else {
            commit("SET_ERROR", res.Error);
          }
          commit("SET_LOADING");
        });
    }
  },
  getters: {
    orderedMovies: state => {
      return state.movies.sort((a, b) => {
        if (a[state.sortBy] > b[state.sortBy]) {
          return -1;
        }
        if (a[state.sortBy] < b[state.sortBy]) {
          return 1;
        }
        return 0;
      });
    }
  },
  modules: {}
});
